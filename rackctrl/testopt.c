/** 
 * @file testopt.c
 * @brief test getopt functions
 * @author kaikuo.zhuo@cern.ch
 * @date 2007-04-27
 * 
 * $Log$
 */
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

void print_usage(char *prog)
{
	printf("Simple help now.\n");
}

int main(int argc, char *argv[])
{
	int c;
	int digit_optind = 0;
	int option_index=0;
	static char *short_options = "cd:hvq";
	static struct option long_options[]={
		{"verbose", 0, 0, 'v'},
		{"help", 0, 0, 'h'},
		{"control", 0, 0, 'c'},
		{"dns", 1, 0, 'd'},
		{"debug", 0, 0, 0},
		{"quiet", 0, 0, 'q'},
		{0, 0, 0, 0},
	};


	while(1){
		int this_option_optind = optind?optind:1;
		c = getopt_long(argc, argv, short_options,
				long_options, &option_index);
		if (c == -1)
			break;
		switch (c) {
			case 0:
				printf ("option %s", long_options[option_index].name);
				if (optarg)
					printf (" with arg %s", optarg);
				printf ("\n");
				break;
			case 'v':
				printf("verbose mode\n");
				break;
			case 'q':
				printf("quiet mode\n");
				break;
			case 'd':
				printf("dns node");
				break;

			case 'h':
				if (optind != argc){
					printf("Help option can only be use alone");
					exit(0);
				}
				print_usage(argv[0]);
				break;

			default:
				printf ("?? getopt returned character code 0%o ??\n", c);
		}
	}

	if (optind < argc) {
		printf ("non-option ARGV-elements: ");
		while (optind < argc)
			printf ("%s ", argv[optind++]);
		printf ("\n");
	}

	exit (0);
}


