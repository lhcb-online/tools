/** 
 * @file rackctrl.c
 * @brief rack controlling through DIM
 * 
 * @author kaikuo.zhuo@cern.ch
 * @date 2007-04-20
 *
 * $Log: rackctrl.c,v $
 * Revision 1.6  2007/05/08 13:46:24  kaikuo
 * Fix some small bug. And make dim server more gracer to operate now.
 * And, make concurrent crate operation available(modify machenism of
 * result update))
 *
 * Revision 1.5  2007/05/02 16:49:30  kaikuo
 * Add a single verification function.(static table in program)
 *
 * Revision 1.4  2007/05/02 09:53:29  kaikuo
 * Set infdaia01w as default DIM_DNS_NODE
 *
 * Revision 1.3  2007/04/27 15:11:00  kaikuo
 * More gracer version. Reserver some interface, some extra features can be
 * added in coming days. Maybe re-parse the result string?
 *
 * Revision 1.2  2007/04/27 14:20:19  kaikuo
 * Add some options to the program. You can specific dns_node now. Some error check mechanism is imported to make itself more robust
 *
 * Revision 1.1.1.1  2007/04/27 12:41:38  kaikuo
 * A simple version to support normal operation.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>

#include <dic.h>
#include "color.h"
#include "lhcb_auth.h"

#define CMD_N 32
#define RESULT_N 2048
#define M 2
#define DIM_HOST "ecs01"
#define RC_SERVICENAME "ecs01"
#define RC_SERVICE "RackCtrl"
#define RC_RESULT "RackCtrl/result"
#define RC_RET "RackCtrl/retCode"
#define RC_CMD "RackCtrl/cmd"

#define _version "0.92"
#define _versionstr "Rackctrl version"_version", build at "__TIME__ ", "__DATE__ \
"\n(C)2007, LHCb Online\nThis software is for controlling racks(as well as \
crates) remotely at Pit 8.\n\n"

//#define _DEBUG_

/** Global variable */
int gFlag=0;
int gCmd=0;
int gOptDebug=0;
int gOptVerbose=0;
int gOptDns=0;
int gOptQuiet=0;

/** DIM command/service data*/
char rc_cmd[CMD_N]; /* C:32 */
unsigned char rc_ret[4]; /* C:1 */
char rc_result[RESULT_N]; /* C:2048 */

static const char sUsage[]=
F_BOLD"Usage:"F_END"\n\trackctrl [options] <commands> [crate [crate2] ...]\n"
"[options]\n"
"\t-d,--dns dns_node   set the current dns node\n"
"\t-h,--help           print the help message\n"
"\t-v,--verbose        echo all message in verbose mode\n"
"\t   --version        print current version\n"
"<commands>\n"
"\tstart               turn on the crate\n"
"\tstop                turn off the crate\n"
"\treset               reset the VME bus off the crate\n"
"\tstatus              get the status of the crate\n"
"\tlist                get a full list of crate\n"
"\tupdate              update the dim service\n"
;

enum {
	CMD_START,
	CMD_STOP,
	CMD_RESET,
	CMD_STATUS,
	CMD_LIST,
	CMD_UPDATE,
	CMD_ERROR
}command_code;

static char *command_str[]={
	"start",
	"stop",
	"reset",
	"status",
	"list",
	"update",
};

int get_cmdCode(char *cmd)
{
	int i;
	for (i=0; i<sizeof(command_str)/sizeof(char*); i++){
		if (strcasecmp(cmd, command_str[i]) == 0)
			return i;
	}
	return CMD_ERROR;
}

void print_usage(char *progname)
{
	printf(sUsage);
	printf(F_BOLD"\nExample:%s \n\t %s status\n", F_END,
			progname);
	printf("\n"F_BOLD"Note:"F_END
			"\n\tYou can use a part of the crate name if it is unique. e.g. you can use D02U instead of D3D02U "
			"if there is only one *D02U in current machine.\n\t"
			F_GREEN"On"F_END" means power on, "
			F_RED"Off"F_END" means power off, and "
			F_YELLOW"Down"F_END" means not connected\n"
			);
}

/** 
 * @brief result_handler handler for geting result from DIM server
 * 
 * @param tag 
 * @param buffer 
 * @param size 
 */
void result_handler(long *tag, int *buffer, int *size)
{
	if (gFlag == 0 ) {
		return;
	}

	memcpy(rc_result, buffer, *size);
	if (gOptDebug)
		printf("\nResult from Wiener:\n%s\n", rc_result);
}

void print_result(char *result, char ret)
{
	int i=0, len=strlen(result);
	if (gCmd == CMD_STATUS){
		for (i=0; i<len; i++)
		{
			if (result[i] == 'O'){
				if (i+1<len && result[i+1]=='n'){
					printf(F_GREEN"On"F_END);
					i+=1;
					continue;
				}
				if (i+2<len && result[i+1]=='f' &&
						result[i+2]=='f'){
					printf(F_RED"Off"F_END);
					i+=2;
					continue;
				}
			}
			if (result[i] != ':')
				putchar(result[i]);
			else
				printf(" is ");
		}
		return;
	}
	printf("%s\n", result);
}

/** 
 * @brief ret_handler for getting return code from DIM server
 * 
 * @param tag 
 * @param buffer 
 * @param size 
 */
void ret_handler(long *tag, int *buffer, int *size)
{
	//Check service first;
	int id = dic_get_conn_id ();

	if (gOptDebug)
		printf("Connection ID: %d\n", id);

	if (id == 0){
		printf("DIM service \"RackCtrl\" not found on current dns node! Please check!\n");
		exit(1);
	}
	if (gFlag++ < M) return;

	memcpy(&rc_ret, buffer, *size);
	if (gOptVerbose) {
		gOptDebug = 1;
		printf("Status of RackCtrl: ");
	}
	switch(rc_ret[0]){
		case 'O':
			if (gOptVerbose) printf("OK\n");
			print_result(rc_result, rc_ret[0]);
			exit(0);
			break;
		case 'F':
			if (gOptVerbose) printf("Failed\n");
			print_result(rc_result, rc_ret[0]);
			exit(0);
			break;
		case 'E':
			if (gOptVerbose) {
				printf("Error\n");
				printf("Message from server: %s", rc_result);
			}
			break;
		case 'S':
			if (gOptVerbose) {
				printf("Start a command, waiting...\n");
				printf("Message from server: %s", rc_result);
			}
			break;
		default:
			if (gOptVerbose) {
				printf("Unknown %2X\n", rc_ret[0]);
				printf("Message from server: %s", rc_result);
			}
			break;
	}
}

/** 
 * @brief error_handler onece any error occurs
 * 
 * @param severity 
 * @param errcode 
 * @param message 
 */
void error_handler(int severity, int errcode, char *message)
{
	printf("DIM ERROR: %s\n", message);
}

//int check_dimService()
//{
//	//int conn_id = dic_get_server();
//	//char *services = dic_get_server_services(conn_id);
//#ifdef _DEBUG_
//	printf("Getting all service on server:\n%s\n", "test");
//#endif
//	return 0;
//}

int main(int argc, char *argv[])
{
	int ret, i;
	int c;
	//int digit_optind = 0;
	int option_index=0;
	char dns_node[32];
	static char *short_options = "cd:hvq";
	static struct option long_options[]={
		{"help", 0, 0, 'h'},
		{"verbose", 0, 0, 'v'},
		{"version", 0, 0, 0},
		{"control", 0, 0, 'c'},
		{"dns", 1, 0, 'd'},
		{"quiet", 0, 0, 'q'},
		{0, 0, 0, 0},
	};

#ifdef _DEBUG_
	gOptVerbose = 1;
#endif

	if (argc <= 1){
		print_usage(argv[0]);
		return 0;
	}

	dic_set_dns_node(DIM_HOST);

	while(1){
		//int this_option_optind = optind?optind:1;
		c = getopt_long(argc, argv, short_options,
				long_options, &option_index);
		if (c == -1)
			break;
		switch (c) {
			case 0:
#ifdef _DEBUG_
				printf ("option %s", long_options[option_index].name);
				if (optarg)
					printf (" with arg %s", optarg);
				printf ("\n");
#endif
				if (strcasecmp(long_options[option_index].name, "version") == 0){
					printf(_versionstr);
					exit(0);
				}
				break;
			case 'v':
				gOptVerbose = 1;
				break;
			case 'q':
				gOptQuiet = 1;
				break;
			case 'd':
				gOptDns = 1;
				strcpy(dns_node, optarg);
				if (dic_set_dns_node(dns_node))
					;
				else
					printf("Setting dns node to %s error!", dns_node);
#ifdef _DEBUG_
				printf("new dns node: %s \n", dns_node);
#endif
				break;

			case 'h':
				if (optind != argc){
					printf("Help option can only be use alone");
				}
				print_usage(argv[0]);
				exit(0);
				break;

			default:
				printf ("Unknown options. %c\n", c);
				exit(1);
		}
	}

	if (optind == argc){
		printf("No command specified!\n");
		exit(1);
	}
#ifdef _DEBUG_
#endif 
	gCmd = get_cmdCode(argv[optind]);
	if (gCmd == CMD_ERROR)
	{
		printf("Unknown command, please type \"rackctrl\" for help. \n");
		return 0;
	}

	//make sure it's empty
	memset(&rc_cmd, 0, CMD_N);
	memset(&rc_result, 0, sizeof(rc_result));
	rc_ret[0] = 'O';

	uid_t uid;
	int count=0;
	gFlag = 0;
	if (gCmd == CMD_START 
			|| gCmd == CMD_STOP
			|| gCmd == CMD_RESET){
		gFlag = 1;
		uid = geteuid();
	}

	for (i = optind; i<argc; i++){
		if (gFlag == 1
				&& i>optind){
#ifdef _DEBUG_
			printf("Param: %s\n", argv[i]);
#endif
			if (auth_crate(argv[i], uid) != 0){
				printf("%s not a valid crate\n", argv[i]);
				continue;
			}
			count++;	///< if a valid crate, ++
		}

		strcat(rc_cmd, argv[i]);
		strcat(rc_cmd, " ");
	}

	if (gFlag && count==0) return 1;	///< for the three commands, if no crate, return

	dic_add_error_handler(error_handler);

	gFlag = 0;
	//ret = dic_cmnd_service(RC_CMD, "clear", CMD_N);

	dic_info_service(RC_RET, MONITORED, 0, &rc_ret, sizeof(rc_ret), ret_handler, 0, NULL, 0);
	dic_info_service(RC_RESULT, MONITORED, 0, &rc_result, sizeof(rc_result), result_handler, 0, NULL, 0);
	/* dic_cmnd_callback(RC_RESULT, &rc_result, sizeof(rc_result), result_handler, 0);*/
	/* dic_cmnd_callback(RC_RET, &rc_ret, sizeof(rc_ret), result_handler, 0);*/

	ret = dic_cmnd_service(RC_CMD, (char*)rc_cmd, CMD_N);
	if (gOptQuiet)
		return 0;
	gFlag = 1;
#ifdef _DEBUG_
	printf("Ret = %d\n", ret);
#endif

	sleep(10);
	fprintf(stderr, "ERROR: No update, timed out.");

	return 0;
}
