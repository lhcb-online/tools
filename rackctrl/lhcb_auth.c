/** 
 * @file lhcb_auth.c
 * @brief autherize user from LHCB
 * @author kaikuo.zhuo@cern.ch
 * @date 2007-05-02
 *
 * $Log: lhcb_auth.c,v $
 * Revision 1.1  2007/05/08 13:49:11  kaikuo
 * supply a simple authentication for users.
 * Get the group info of the euid, and check with
 * the group(online, velo, ot, st, etc)
 *
 *
 */
#include "lhcb_auth.h"

#define GROUP_TRG		1011
#define GROUP_HLT		1006
#define GROUP_CALO		1009
#define GROUP_MUON		1010
#define GROUP_ONLINE	1000
#define GROUP_OT		1004
#define GROUP_LB		1008
#define GROUP_VELO		1001
#define GROUP_ST		1005
#define GROUP_ADMIN		1003
#define GROUP_DAQ		1021
#define GROUP_RICH		1002
#define GROUP_GCS		1007
#define GROUP_IT		GROUP_ONLINE

static char *lb_admins[]=
{
	"kaikuo",
	"niko",
	"vbobillier",
};

typedef struct tagItem{
	char *name;
	gid_t gid;
}rackitem_t;

#define ITEMNAME_LEN 6
const rackitem_t lb_racks[]=
{
	{"D3A01U"	, GROUP_MUON},
	{"D3A01L"	, GROUP_MUON},
	{"D3A03U"	, GROUP_MUON},
	{"D3A03L"	, GROUP_MUON},
	{"D3A04U"	, GROUP_MUON},
	{"D3A04L"	, GROUP_MUON},
	{"D3B01U"	, GROUP_CALO},
	{"D3B01L"	, GROUP_CALO},
	{"D3B02U"	, GROUP_CALO},
	{"D3B02L"	, GROUP_CALO},
	{"D3B07L"	, GROUP_ONLINE},
	{"D3B07U"	, GROUP_ONLINE},
	{"D3B08L"	, GROUP_ONLINE},
	{"D3B08U"	, GROUP_ONLINE},
	{"D3B09U"	, GROUP_ONLINE},
	{"D3B09L"	, GROUP_ONLINE},
	{"D3C01U"	, GROUP_RICH},
	{"D3C01L"	, GROUP_RICH},
	{"D3C02U"	, GROUP_RICH},
	{"D3C02L"	, GROUP_RICH},
	{"D3C04U"	, GROUP_RICH},
	{"D3C05U"	, GROUP_RICH},
	{"D3C05L"	, GROUP_RICH},
	{"D3D01L"	, GROUP_OT},
	{"D3D01U"	, GROUP_OT},
	{"D3D02L"	, GROUP_OT},
	{"D3D02U"	, GROUP_OT},
	{"D3D03U"	, GROUP_OT},
	{"D3D04U"	, GROUP_OT},
	{"D3D05U"	, GROUP_OT},
	{"D3D07U"	, GROUP_IT},
	{"D3D07L"	, GROUP_IT},
	{"D3D08U"	, GROUP_IT},
	{"D3D09U"	, GROUP_IT},
	{"D3D09L"	, GROUP_IT},
	{"D3E01U"	, GROUP_VELO},
	{"D3E02U"	, GROUP_VELO},
	{"D3E02L"	, GROUP_VELO},
	{"D3E03U"	, GROUP_VELO},
	{"D3E03L"	, GROUP_VELO},
	{"D3E04U"	, GROUP_VELO},
	{"D3E04L"	, GROUP_VELO},
	{"D3E07U"	, GROUP_TRG},
	{"D3E07L"	, GROUP_TRG},
	{"D3E08U"	, GROUP_TRG},
	{"D3E09U"	, GROUP_TRG},
	{"D3E09L"	, GROUP_TRG},
	{"B1B01U"	, GROUP_CALO},
	{"B1B01L"	, GROUP_CALO},
	{"B1C01U"	, GROUP_OT},
};

int get_case_index(const char *item, const char *list[])
{
	int i;
	for (i=0; list[i]; i++)
	{
		if (strcasecmp(item, list[i]) == 0)
			return i;
	}
	return -1;
}

int get_index(const char *item, char *list[])
{
	int i;

	for (i=0; list[i]; i++)
	{
		if (strcmp(item, list[i]) == 0)
			return i;
	}
	return -1;
}

gid_t lb_get_crate_group(const char *crate )
{
	int i;
	int len = strlen(crate);
	int offset = 0;
	int offset2 = 0;
	char *list;
	if (len >= ITEMNAME_LEN) {
		offset = len-ITEMNAME_LEN;
		len = ITEMNAME_LEN;
	}else{ 
		offset2 = ITEMNAME_LEN - len;
	}
#ifdef _DEBUG_
	//printf(" %d %d %d\n", offset, sizeof(rackitem_t), sizeof(lb_racks));
#endif

	for (i = 0; i<sizeof(lb_racks)/sizeof(rackitem_t); i++){
		if (strncasecmp(crate+offset, lb_racks[i].name+offset2, len)==0){
#ifdef _DEBUG_
			printf("Find a match, crate: %d,%s\n", i, lb_racks[i].name);
#endif
			return lb_racks[i].gid;
		}
	}
#ifdef _DEBUG_
	printf("Crate %s not found\n", crate);
#endif
	return 0;
}

int lb_check_group(uid_t uid, gid_t gid)
{
	int ng=0;
	int i;
	gid_t *groups=NULL;
	char cgrpname[16];

	if (gid == 0) return 1;

	struct passwd *pwd = getpwuid(uid);
	char *name = pwd->pw_name;
	struct group *grp = getgrgid(gid);
	strcpy(cgrpname, grp->gr_name);

#ifdef _DEBUG_
	printf("Crate Group: %s\n", cgrpname);
	printf("Verifying user privilege of %s...\n", name);
#endif

	if (get_index(name, lb_admins) != -1){
#ifdef _DEBUG_
		printf("Welcome, administrator %s!\n", name);
#endif
		return 0;
	}

	grp = getgrgid(pwd->pw_gid);
	char *ugrpname = grp->gr_name;
#ifdef _DEBUG_
	printf("Name : %s, User ID: %d, Primary Group ID: %d, Group Name :%s\n", 
			name, uid, pwd->pw_gid, ugrpname);
#endif

	if (getgrouplist(name, pwd->pw_gid, NULL, &ng) < 0) {
#ifdef _DEBUG_
		printf("Number of groups %d\n", ng);
#endif
		groups = (gid_t *) malloc(ng * sizeof (gid_t));
		getgrouplist(name, pwd->pw_gid, groups, &ng);
	}

	for(i = 0; i < ng; i++){
#ifdef _DEBUG_
		printf("groups: %i,%d\n", i, groups[i]);
#endif
		if (gid == groups[i] 
				|| GROUP_ADMIN == groups[i]
		   ){
#ifdef _DEBUG_
			grp = getgrgid(groups[i]);
			printf("Find a match, groups: %i,%d\n", i, groups[i]);
			printf("Welcome, %s users.\n", grp->gr_name);
#endif
			return 0;
		}
		//		else if ( GROUP_ADMIN == groups[i]){
		//#ifdef _DEBUG_
		//			grp = getgrgid(gid);
		//			printf("Welcome, Administrator %s .\n", groups);
		//#endif
		//		}

	}
//#ifdef _DEBUG_
	printf("Hi, %s, your are %s user, you do not have privilege to operate %s crates.\n",
			pwd->pw_name, ugrpname, cgrpname);
//#endif
	return 1;
}

int auth_crate(const char *crate, uid_t uid)
{
	int gid = lb_get_crate_group(crate);
#ifdef _DEBUG_
	printf("%s, %d", crate, gid);
#endif
	return lb_check_group(uid, gid);
}
