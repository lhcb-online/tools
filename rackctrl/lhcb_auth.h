/** 
 * @file lhcb_auth.h
 * @brief header for autherization
 * @author kaikuo.zhuo@cern.ch
 * @date 2007-05-02
 */
#ifndef  _LHCB_AUTH_H
#define  _LHCB_AUTH_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pwd.h>
#include <grp.h>

int auth_crate(const char *crate, uid_t uid);

#define CRATE_OK 0
#define CRATE_NOT_EXIST 1
#define CRATE_NOT_ENOUGH 2

#endif   /*_LHCB_AUTH_H*/

