#ifndef  _COLOR_H
#define  _COLOR_H

#define CLR_HEADER "\033["
#define F_RED 	CLR_HEADER";;31m"
#define F_GREEN CLR_HEADER";;32m"
#define F_YELLOW CLR_HEADER";;33m"
#define F_END 	CLR_HEADER"0m"
#define F_BOLD 	CLR_HEADER"1m"

#endif   /*_COLOR_H*/
